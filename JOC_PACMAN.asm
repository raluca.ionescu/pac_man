.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc

includelib canvas.lib
extern BeginDrawing: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
window_title DB "PACMAN",0
area_width EQU 850
area_height EQU 760
area DD 0
i db 0
j db 0
i_pacman dd 0
j_pacman dd 0
counter DD 0 ; numara evenimentele de tip timer
nr_c dd 21
aux dd 0
scor dd 0
lives db 3
directie dd 0
poz_ghost dd 178
liber_sus db 0
liber_jos db 0
liber_stanga db 0
liber_dreapta db 0
reset db 0
i_pac dd 0
j_pac dd 0
i_ghost dd 0
j_ghost dd 0
play db 0
won db 0

numar dd 0

arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20
lungime_mat EQU 20
lungime EQU 21
inaltime_mat EQU 22
x0 EQU 50
y0 EQU 50
xo EQU 700
yo EQU 300
lungime_op EQU 3
inaltime_op EQU 2
lung_op EQU 2
poz dd 346
miscare db 0
simbol EQU 30




symbol_width EQU 10
symbol_height EQU 20
include digits.inc
include letters.inc
include matrice.inc
include elem.inc
include operatie.inc
include pacman.inc
include house.inc
include spatiu.inc
include ghosts.inc
include fruit.inc
include matrice_noua.inc


;aici declaram date

.code
; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli   area
; arg3 - pos_x
; arg4 - pos_y


make_block proc
	push ebp
	mov ebp, esp
	pusha
	mov eax, [ebp+arg1]  ; simbolul de afisat
	mov ecx, [ebp+24]
	
	cmp eax, 3
	je pacman_desen
	cmp eax, 1
	je bloc_desen
	cmp eax, 2
	je casa
	cmp eax, 8
	je puncte
	cmp eax, 4
	je ghost_red
	cmp eax, 5
	je ghost_pink
	cmp eax, 0
	je liber
	cmp eax,6
	je sageata
	cmp eax,9
	je sageata
	cmp eax, 11
	je pac_left
	cmp eax, 12
	je pac_up
	cmp eax, 13
	je pac_down
	cmp eax,7
    je portocala	
	
pacman_desen:
	lea esi, pacman
	jmp bucla_simbol_linii_02
bloc_desen:
    lea esi, elem
	jmp bucla_simbol_linii_02
casa:
	lea esi, house
	jmp bucla_simbol_linii_02
puncte:
	lea esi, spatiu
	jmp bucla_simbol_linii_02
ghost_red:
    lea esi, ghosts
	jmp bucla_simbol_linii_02
ghost_pink:
     lea esi, ghosts
	 add esi,900
	 jmp bucla_simbol_linii_02
liber:
	lea esi, spatiu
	add esi , 900
	jmp bucla_simbol_linii_02
sageata:
    lea esi, elem
	jmp bucla_simbol_linii_02
pac_left:
   lea esi, pacman
   add esi,900
   jmp bucla_simbol_linii_02
pac_up:
	lea esi,pacman
	add esi,1800
	jmp bucla_simbol_linii_02	

pac_down:
    lea esi,pacman
	add esi,2700
	jmp bucla_simbol_linii_02

portocala:
    lea esi, fruit
	jmp bucla_simbol_linii_02
  
bucla_simbol_linii_02:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, simbol
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, simbol
	
bucla_simbol_coloane_02:
	cmp byte ptr [esi], 1
	je simbol_pixel_bleu
	cmp byte ptr [esi], 0
	je simbol_pixel_albastru
	cmp byte ptr [esi] , 3
	je simbol_pixel_galben
	cmp byte ptr [esi], 2
	je simbol_pixel_mov
	cmp byte ptr [esi], 5
	je simbol_pixel_alb_
	cmp byte ptr [esi] , 4
	je simbol_pixel_rosu
	cmp byte ptr [esi], 6
	je simbol_pixel_roz
	cmp byte ptr [esi],7
	je simbol_pixel_portocaliu
	cmp byte ptr [esi], 8
	je simbol_pixel_verde
	mov dword ptr [edi], 0
	jmp simbol_pixel_next_02

simbol_pixel_portocaliu:
  	mov dword ptr [edi],0FFA726h
    jmp simbol_pixel_next_02
simbol_pixel_verde:
  	mov dword ptr [edi],0388E3Ch
    jmp simbol_pixel_next_02
simbol_pixel_bleu:
	mov dword ptr [edi], 01E88E5h     
    jmp simbol_pixel_next_02
simbol_pixel_albastru:
	mov dword ptr [edi], 0000066h
	jmp simbol_pixel_next_02
simbol_pixel_mov:
	mov dword ptr [edi], 07E57C2h
	jmp simbol_pixel_next_02
simbol_pixel_galben:
	mov dword ptr [edi], 0FFEB3Bh
	jmp simbol_pixel_next_02
simbol_pixel_alb_:
    mov dword ptr [edi], 0FAFAFAh
	jmp simbol_pixel_next_02
simbol_pixel_rosu:
    mov dword ptr [edi], 0FF0000h
	jmp simbol_pixel_next_02
simbol_pixel_roz:
    mov dword ptr [edi], 0FF33CCh
	jmp simbol_pixel_next_02	
 
simbol_pixel_next_02:

	inc esi
	add edi, 4
	dec ecx
	jne bucla_simbol_coloane_02
	pop ecx
	dec ecx
	jne bucla_simbol_linii_02

	popa
	mov esp, ebp
	pop ebp
	ret
	
make_block endp

make_block_macro macro symbol, drawArea, x, y , dimensiune
	push dimensiune
	push y
	push x
	push drawArea
	push symbol
	call make_block
	add esp, 20
endm



make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0F4511Eh
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi],0000066h
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp



; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm



linie_verticala macro drawArea, x, y, len
LOCAL vf, final
	push eax
	push ecx
	push ebx
	
	mov eax, x
	mov ebx,850
	mul ebx
	add eax, y
	shl eax, 2
	mov ecx, 0
vf:
    add eax, 3399  ;  sizelinie
	mov EBX, drawArea
	add ebx, ecx 
	mov dword ptr [ebx+eax], 000000h
	inc ecx
	inc ecx
	cmp ecx, len
	je final
	loop vf
final:
	pop ebx
	pop ecx
	pop eax
endm

linie_orizontala macro drawArea, x, y, len
LOCAL vf, final
	push eax
	push ecx
	push ebx
	
	mov eax, 0
	mov eax, x
	mov ebx, 850
	mul ebx
	add eax, y
	shl eax, 2
	mov ecx, 0
vf:
    
	mov EBX, drawArea
	add ebx, ecx
	mov dword ptr [ebx+eax], 000000h
	inc ecx
	inc ecx
	cmp ecx, len
	je final
	loop vf
final:
	pop ebx
	pop ecx
	pop eax
endm


; functia de desenare - se apeleaza la fiecare click
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 1
	jz evt_click
	cmp eax, 2
	jz evt_timer ; nu s-a efectuat click pe nimic
	;mai jos e codul care intializeaza fereastra cu pixeli albi
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	
	
	;colorarea intregului ecran
	mov ecx,eax
	mov eax, 0000066h
	mov edi, area
	rep stosd;
	
	shl eax, 2
	push eax
	push 255
	push area
	call memset ;alocam memorie
	add esp, 12
	jmp afisare_litere
	
evt_click:
  
	mov ebx, [ebp+arg2]   ;-x
	
	mov edx, [ebp+arg3]   ;-y
	
	 
	; verificam daca s-a dat pe play again
	cmp ebx , 720
	jl joc
	cmp ebx, 800
	jg joc
	cmp edx, 400
	jl joc
	cmp edx, 420
	jg joc
	
	;s-a dat click
	 mov reset,1
	 jmp final_joc
joc:    
	mov eax , edx   ; i
	sub eax, yo
	mov ecx , simbol
	mov edx, 0
	div ecx
	
	push eax
	
	mov eax, ebx
	sub eax, xo
	mov ecx,simbol
	mov edx, 0
	div ecx
	
	mov ecx, eax   ;eax = i  ecx = j
	pop eax 
	mov edx, lungime_op
	mul edx
	add eax, ecx
	
	
	mov cl, operatie[eax]
	mov miscare,cl        		; miscarea contine elementul din matricea operatie. 
	

evt_timer:

    cmp scor, 300
    jge afisare_litere    ; daca s-a castigat vrem sa se reia jocul 
   
	inc counter
	
	; mov  eax, counter
	; mov numar, 3
	; mov edx, 0
	; div numar

	; cmp edx, 0
	; je move_ghost
	; jmp dir
 
;move red ghost	
move_ghost:
    push ebx				; (poz_ghost - poz) / lungime
	push eax
	push edx
	
	mov eax, poz_ghost
	mov edx, 0
	mov ebx, lungime			
	div ebx             ; catul = linia,  restul = coloana
	
	mov i_ghost , eax
	mov j_ghost, edx
	
	mov eax, poz
	mov edx, 0
	mov ebx, lungime	
    div ebx
	
	mov i_pac, eax
	mov j_pac, edx
	
coloane:
    mov ebx, i_pac
	cmp i_ghost, ebx
	jl jos
	jg sus
	;avem pe aceeasi linie
	
linii:
    mov ebx, j_pac
	cmp j_ghost, ebx
	jl dreapta_lin
	jg stanga_lin
   jmp dir
jos:
	;mutam fantoma in jos
	mov eax, poz_ghost
	mov matrice[eax],8      ; in urma ei , fantoma lasa puncte
	add eax,lungime
	cmp matrice[eax],1
	je dr_stg
	cmp matrice[eax],2
	je dr_stg
	
	
	mov matrice[eax],4
	mov poz_ghost, eax
	
    jmp dir
dr_stg: 
	mov ebx, j_ghost
	cmp j_pac, ebx
	jl stanga
	jg dreapta
	je stanga
    jmp dir
	
sus:
   mov eax, poz_ghost
   sub eax,lungime
   cmp matrice[eax],1
   je dr_stg
   cmp matrice[eax],2
   je dreapta
  
   ;mutam in sus
   mov matrice[eax],4
   mov poz_ghost,eax
   add eax, lungime
   mov matrice[eax],8
   jmp dir

dreapta:
    mov eax, poz_ghost
	inc eax
    cmp matrice[eax], 1
    je stanga
    cmp matrice[eax], 2
    je stanga

   ;mutam in dreapta

   mov matrice[eax],4
   mov eax,poz_ghost
   mov matrice[eax], 8
   inc eax
   mov poz_ghost, eax
   jmp dir
   
stanga:
    mov eax, poz_ghost
	dec eax
    cmp matrice[eax], 1
	je dreapta
	cmp matrice[eax], 2
	je dreapta
	;mutam in stanga
	mov matrice[eax], 4
	mov eax, poz_ghost
	mov matrice[eax], 8
	dec eax
	mov poz_ghost, eax
	jmp dir

	
dreapta_lin:
    mov eax, poz_ghost
	inc eax
   cmp matrice[eax], 1
   je stanga_lin
   cmp matrice[eax], 2
   je stanga_lin
  
   ;mutam in dreapta
   mov matrice[eax],4
   mov eax,poz_ghost
   mov matrice[eax], 8
   inc eax
   mov poz_ghost, eax
   jmp dir

stanga_lin:	
    mov eax, poz_ghost
	dec eax
    cmp matrice[eax], 1
	je jos_lin
	cmp matrice[eax], 2
	je jos_lin
	;mutam in stanga
	mov matrice[eax], 4
	mov eax, poz_ghost
	mov matrice[eax], 8
	dec eax
	mov poz_ghost, eax
	jmp dir	
jos_lin:
	;mutam fantoma in jos
	mov eax, poz_ghost
	mov matrice[eax],8
	add eax,lungime
	cmp matrice[eax],1
	je sus
	cmp matrice[eax],2
	je sus
	
	
	mov matrice[eax],4
	mov poz_ghost, eax
	
	
dir:	
    
	pop ebx
	pop eax
	pop ebx
		
	;verificam ce mutare trebuie sa facem
    cmp miscare, 6
	je move_left
	cmp miscare, 9
	je move_r
	cmp miscare, 1
	je move_up
	cmp miscare, 2
	je move_down
	
	jmp final
	
move_r:							; in directie vom pune valoarea ce trebuie adunata pentru a determina 
   mov directie,1						; urmatoarea patratica in care pacman trebuie sa se duca
   jmp muta
move_left:
   mov directie, -1
   jmp muta
move_up:
   mov directie, -21
   jmp muta
move_down:
   mov directie, 21
   jmp muta
	
muta:
							;verificam daca se poate face miscarea

    mov eax,poz
	
    add eax,directie
	
	mov ebx, poz_ghost
	cmp poz, ebx
	je update
	
	cmp matrice[eax] , 8       ; daca avem 8 , atunci avem spatiu cu punct , deci facem mutarea si adunam la scor
	je plus_scor_r
	cmp matrice[eax], 0        ; daca avem 0 , atunci avem doar spatiu, realizam mutarea
	je next_r
	cmp matrice[eax], 4       ; am dat de fantoma rosie 
	je vieti
	cmp matrice[eax],7         ; avem spatiu cu un fruct, la scor se aduna 100
	je plus_scor_100
    jmp final
	
plus_scor_100:
	add scor,100
	jmp next_r
plus_scor_r:
   inc scor	
next_r:
    cmp miscare,6       ; verificam ce mutare se face pentru a vedea ce orientarea trebuie sa aiba pacman la afisare
	je pacman_left
	cmp miscare,2
	je pacman_down
	cmp miscare,1
	je pacman_up
	cmp miscare,9
	je pacman_right
	jmp continua
pacman_left:
	mov matrice[eax],11
	jmp continua
pacman_up:
     mov matrice[eax],12
    jmp continua
pacman_down:
    mov matrice[eax],13
    jmp continua
pacman_right:
    mov matrice[eax],3
    jmp continua
   
continua:	
 
	sub eax, directie       ; realizam mutarea, si punem 0 unde se afla pacman
	mov matrice[eax], 0
	add eax, directie
	mov poz, eax
    jmp final

 vieti:
    cmp miscare, 6
	je left 
	cmp miscare, 9
	je right 
	cmp miscare, 1
	je up
	cmp miscare, 2
	je down

left:
    inc eax 
	jmp update
	
right:
	dec eax
	jmp update
	
up:
    add eax, lungime
    jmp update
  
down:
    sub eax, lungime
    jmp update 
	
update:	

  
    cmp lives, 0             ; daca s-au termiant vietile vom afisa mesajul de Game Over
	je final
	mov eax, poz
    mov matrice[eax], 0
	mov eax, poz_ghost
	mov matrice[eax], 0
	mov miscare, 0            ;resetam miscarea pe 0 . Pacman o sa stea
	dec lives
	cmp lives ,0
	je game_over
	mov eax, 346              ; pacman revine la pozitia initiala
	mov poz, eax
	mov matrice[eax], 3
	mov eax,178
	mov poz_ghost, eax
	mov matrice[eax],4
   
final: 
    inc counter
	
fructe:
    cmp counter, 20           ;dupa obtinerea unui anumit scor vor aparea pe ecran fructe, de 100 de puncte
	je show_fruit
	cmp counter, 150
	je show_fruit
    jmp teleportare
show_fruit:
    push eax
	mov eax, 65
	add eax,counter
	mov matrice[eax],7
	pop eax
    jmp teleportare
	
teleportare:	
    cmp poz, 230
	je teleportare1
	cmp poz, 210
	je teleportare2
	jmp afisare_litere
teleportare1:
    cmp miscare, 9
	jne afisare_litere
	mov matrice[eax],0
	sub eax, 20
	mov poz,eax
	mov matrice[eax], 3
	jmp afisare_litere
teleportare2:
    cmp miscare, 6
	jne afisare_litere
	mov matrice[eax],0
	add eax, 20
	mov poz,eax
	mov matrice[eax], 3
	jmp afisare_litere
final_joc:
   mov reset, 0
   mov scor  , 0
   mov lives, 3
   push ecx
   push ebx
   mov ecx , 0
   mov poz, 346
   mov poz_ghost, 178
   mov won,0
   mov miscare, 0
   mov counter, 0
creaza_matrice:
   mov bl, matrice_noua[ecx]
   mov matrice[ecx],bl
   inc ecx
   cmp ecx, 462
   jne creaza_matrice
    mov play, 1
   pop ebx
   pop ecx
	
afisare_litere:
	
	;lea esi, matrice
	;calculam inidicii la care se afla cifra in matrice
	; i - ecx ,j - ebx indicii
	mov ebx, 0
	mov edx, 0
	mov ecx, 0
bucla_i:
	
	cmp ecx,inaltime_mat
	je done
	mov ebx, 0

bucla_j:
	
	;cod
	;calculam x = x0 + j*swidth -> eax
	mov eax, ebx
	mov esi , simbol
	mul esi
	add eax, x0  
	
	push eax
	;calculam y = y0 + i*sh  - edx
	
	mov eax, ecx
	mov esi, simbol
	mul esi
	add eax, y0
	mov edx, eax  ; - y-ul
	push edx
     
	;calculam indicele din matrice in edi
	; i * c + j     i=ecx, c = lungime , ebx =j
	mov eax ,ecx
	mov esi, lungime
	mul esi
	add eax, ebx
	mov edi, eax
	
	mov eax, 0
	mov al , matrice[edi]
	
	
	mov edi,eax
	
	pop eax
	pop edx
	
	make_block_macro edi, area, edx, eax , 30

	cmp ebx, lungime_mat
	je bucla_j_done
	inc ebx
	jmp bucla_j
bucla_j_done:
    inc edi
	inc ecx
	jmp bucla_i
done:	


matrice_op :
	mov ebx, 0
	mov edx, 0
	mov ecx, 0
bucla_io:
	
	cmp ecx,inaltime_op
	je doneo
	mov ebx, 0

bucla_jo:
	
	;cod
	;calculam x = x0 + j*swidth -> eax
	mov eax, ebx
	mov esi , simbol 
	mul esi
	add eax, xo
	
	push eax
	;calculam y = y0 + i*sh  - edx
	
	mov eax, ecx
	mov esi, simbol
	mul esi
	add eax, yo
	mov edx, eax  ; - y-ul
	push edx
     
	;calculam indicele din matrice in edi
	; i * c + j     i=ecx, c = lungime , ebx =j
	mov eax ,ecx
	mov esi, lungime_op
	mul esi
	add eax, ebx
	mov edi, eax
	
	mov eax, 0
	mov al , operatie[edi]
    ;add eax, '0'
	
	mov edi,eax
	
	pop eax
	pop edx
	
	make_block_macro edi, area, edx, eax, 30

	cmp ebx, lung_op
	je bucla_j_doneo
	inc ebx
	jmp bucla_jo
bucla_j_doneo:
    inc edi
	inc ecx
	jmp bucla_io
doneo:	

	; ;scriem un mesaj
	make_text_macro 'G', area, 210, 20
	make_text_macro 'E', area, 220, 20
	make_text_macro 'T', area, 230, 20
	
	make_text_macro 'O', area, 250, 20
	make_text_macro 'V', area, 260, 20
	make_text_macro 'E', area, 270, 20
	make_text_macro 'R', area, 280, 20
	
	make_text_macro '3', area, 300, 20
	make_text_macro '0', area, 310, 20
	make_text_macro '0', area, 320, 20
	
	make_text_macro 'P', area, 340, 20
	make_text_macro 'O', area, 350, 20
	make_text_macro 'I', area, 360, 20
	make_text_macro 'N', area, 370, 20
	make_text_macro 'T', area, 380, 20
	make_text_macro 'S', area, 390, 20
	
	make_text_macro 'T', area, 410, 20
	make_text_macro 'O', area, 420, 20
	
	make_text_macro 'W', area, 440, 20
	make_text_macro 'I', area, 450, 20
	make_text_macro 'N', area, 460, 20
	
    make_text_macro 'P', area, 730, 20
	make_text_macro 'A', area, 740,20
	make_text_macro 'C', area, 750, 20
	make_text_macro 'M', area, 760, 20
	make_text_macro 'A', area, 770, 20
	make_text_macro 'N', area, 780, 20
	
	make_text_macro 'S', area, 690, 150
	make_text_macro 'C', area, 700, 150
	make_text_macro 'O', area, 710, 150
	make_text_macro 'R', area, 720, 150
	make_text_macro 'E', area, 730, 150
	
	;afisam scorul pe excran
	mov eax, 0
	mov eax, scor
	
	push ecx
	mov ecx,10
	
	;unitati
	mov edx, 0
	div ecx
	add edx, '0'
	make_text_macro edx, area, 770, 150
	
	;zeci
	mov edx, 0
	div ecx
	add edx, '0'
	make_text_macro edx, area, 760, 150
	
	;sute
	mov edx, 0
	div ecx
	add edx,'0'
	make_text_macro edx, area, 750, 150
	
	pop ecx
	
	make_text_macro 'L', area, 690, 180
	make_text_macro 'I', area, 700, 180
	make_text_macro 'V', area, 710, 180
	make_text_macro 'E', area, 720, 180
	make_text_macro 'S', area, 730, 180
	
	mov eax, 0
	mov al, lives
	add eax, '0'
	cmp eax, 0
	je game_over
	make_text_macro  eax, area, 750, 180
	
	
	mov eax, 0
	mov eax, scor
	
	cmp eax, 300
    jge won_game
	jmp final_draw
	

won_game:
    
	make_text_macro 'Y', area, 720, 250
	make_text_macro 'O', area, 730, 250
	make_text_macro 'U', area, 740, 250
	make_text_macro ' ', area, 750, 250
	
	make_text_macro 'W', area, 760, 250
	make_text_macro 'O', area, 770, 250
	make_text_macro 'N', area, 780, 250
	make_text_macro ' ', area, 790, 250
	mov won, 1
	jmp play_again
 ; afisam mesajul de Game Over. Se pierde atunci cand pacman nu mai are nicio viata
 
game_over:

	make_text_macro 'G', area, 720, 250
	make_text_macro 'A', area, 730, 250
	make_text_macro 'M', area, 740, 250
    make_text_macro 'E', area, 750, 250
	
	make_text_macro 'O', area, 760, 250
	make_text_macro 'V', area, 770, 250
	make_text_macro 'E', area, 780, 250
    make_text_macro 'R', area, 790, 250
	
	cmp reset,1
	je play_again

play_again:
	
    make_text_macro 'P', area, 720, 400
	make_text_macro 'L', area, 730, 400
	make_text_macro 'A', area, 740, 400
    make_text_macro 'Y', area, 750, 400

	make_text_macro 'A', area, 770, 400
	make_text_macro 'G', area, 780, 400
	make_text_macro 'A', area, 790, 400
    make_text_macro 'I', area, 800, 400
    make_text_macro 'N', area, 810, 400
	
	mov won,0
   	mov play, 0

final_draw:
	;chenarul
	
    linie_orizontala area, 50 ,50 , 2520 ; oy, ox
	 linie_orizontala area, 49, 50, 2520
	 linie_orizontala area, 48, 50, 2520
	 linie_orizontala area, 47, 50,  2520
      linie_verticala  area , 50, 50, 660
	 linie_verticala area, 50, 49, 660
	 linie_verticala area, 50, 48, 660
	 linie_verticala area, 50, 680, 660   ;y-ul, x-ul, lungimea 
	 linie_verticala area, 50, 681, 660
	 linie_verticala area, 50, 682, 660
	 linie_orizontala area,710 , 50, 2520
	 linie_orizontala area,711  , 50, 2520
	 linie_orizontala area,712 , 50, 2520
	 
	 linie_verticala area, 300, 730, 60
	 linie_verticala area, 300, 760, 60
	 linie_orizontala area, 330,730, 120
	popa
	mov esp, ebp
	pop ebp
	ret
draw endp

start:
	;alocam memorie pentru zona de desenat
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	call malloc
	add esp, 4
	mov area, eax
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add esp, 20
	;terminarea programului
	push 0
	call exit
end start
